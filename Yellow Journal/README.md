#  ECE 564 Project

App meant for iPad Pro 12.9-inch. To start, simply click on the "+" button in the top right corner and create a journal. A journal must include a name but does not need a password. If a cover isn't picked, the journal will default to having a yellow cover. Clicking on the journal will send you to the entry view, where you are able to draw or write as you please by clicking the buttons in the top right corner. You can add new entries by clicking the "+" in the `primaryView` or by swiping, if you have that option enabled. Have fun!

## Function List
* Pencil/Finger drawing via entries on journals 
* Texboxes with dynamic height available on entries
* Multiple journals available with freedom to choose cover art 
* App has both dark and light mode available via settings
* Pull to add new entry available via settings. When this setting is on, you can swipe up (when not in pencil mode) to add a new entry to your journal.
* Permanent entries available via settings. When this setting is on, you are unable to delete journal entries. 
* Journals can be password protected. 
* Journals can only be deleted when "delete mode" is on, which can be done by pressing the trash can button in the journal view
* Persistent storage via coredata

## Entries
* Using a split view controller, the user can see a list of all their available entries sorted by date created.
* Swipe left on entry inside the table to delete
* To add a new entry, simply tap the add button on the side view or, if pull to add is enable, swipe up when not in pencil mode.
* Journals return to last entry you were in before you left the entry view


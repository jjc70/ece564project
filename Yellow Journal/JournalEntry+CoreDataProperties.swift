//
//  JournalEntry+CoreDataProperties.swift
//  Yellow Journal
//
//  Created by Erik Gregorio on 11/5/20.
//
//

import Foundation
import CoreData


extension JournalEntry {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<JournalEntry> {
        return NSFetchRequest<JournalEntry>(entityName: "JournalEntry")
    }

    @NSManaged public var name: String?
    @NSManaged public var createdDate: Date?
    @NSManaged public var lastModified: Date?
    @NSManaged public var content: Data?
    @NSManaged public var preview: String?

}

extension JournalEntry : Identifiable {

}

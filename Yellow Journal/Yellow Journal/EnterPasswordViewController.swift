//
//  EnterPasswordViewController.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 11/12/20.
//

import UIKit
import CryptoKit

class EnterPasswordViewController: UIViewController {

    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var enterButton: UIButton!
    
    weak var delegate : JournalCollectionViewController? = nil
    
    var journalPassword : String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferredContentSize = CGSize(width: 400, height: 125)
    }
    
    @IBAction func enterButtonAction(_ sender: Any) {
        let isPasswordEqual : Bool = (journalPassword == passwordField.text!)
        if !isPasswordEqual {
            passwordField.placeholder = "Incorrect guess"
            passwordField.text = ""
        } else {
            self.dismiss(animated: true, completion: nil)
            delegate?.presentEntryView()
        }
    }
    
    func setJournalPassword(_ password : String) {
        self.journalPassword = password
    }
}

//
//  JournalCell.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 10/15/20.
//

import UIKit

class JournalCell : UICollectionViewCell {
    //Mark: Properties
    @IBOutlet weak var journalName: UILabel!
    @IBOutlet weak var journalCover: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.journalName.text = nil
        self.journalCover.image = nil
    }
}

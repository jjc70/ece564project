//
//  JournalCoverCollectionViewController.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 11/12/20.
//

import UIKit

protocol CoverDelegate : class {
    func userSelectedCover(cover: UIImage)
}

private let reuseIdentifier = "JournalCoverViewCell"

class JournalCoverCollectionViewController: UICollectionViewController {
    
    @IBOutlet var journalCoverCollectionView: UICollectionView!
    @IBOutlet weak var setCoverButton: UIBarButtonItem!
    
    weak var delegate : CoverDelegate? = nil
    
    private var journalCovers : [UIImage] = [#imageLiteral(resourceName: "redJournal"), #imageLiteral(resourceName: "cyanJournal"), #imageLiteral(resourceName: "greenJournal"), #imageLiteral(resourceName: "yellowJournal"), #imageLiteral(resourceName: "purpleJournal"), #imageLiteral(resourceName: "blueJournal")]
    private var selectedCover : UIImage? = nil
    private var prevIndexPath : IndexPath? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.clearsSelectionOnViewWillAppear = false
        
        setCoverButton.target = self
        setCoverButton.action = #selector(sendCover(sender:))
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return journalCovers.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? JournalCoverViewCell else {
            fatalError("The dequeued cell is not an instance of JournalCoverViewCell")
        }
    
        cell.journalCover.image = journalCovers[indexPath.item]
    
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedCover = journalCovers[indexPath.item]
        let cell = collectionView.cellForItem(at: indexPath)
        
        cell?.layer.borderWidth = 6.0
        cell?.layer.borderColor = UIColor.gray.cgColor
        
        if indexPath != prevIndexPath {
            if let path = prevIndexPath {
                let prevCell = collectionView.cellForItem(at: path)
                prevCell?.layer.borderWidth = 0.0
            }
        }
        
        prevIndexPath = indexPath
    }
    
    @objc func sendCover(sender: UIBarButtonItem) {
        guard let cover = selectedCover else { return }
        
        delegate?.userSelectedCover(cover: cover)
        self.navigationController?.popViewController(animated: true)
    }

}

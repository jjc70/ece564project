//
//  SettingsViewController.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 10/15/20.
//

import Foundation
import UIKit

class SettingsViewController : UITableViewController {
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let settings_UD = UserDefaults.standard

    
    @IBOutlet weak var themes: UIView!
    @IBOutlet weak var statusBar: UIView!
    @IBOutlet weak var toolPosition: UIView!
    @IBOutlet weak var pullToAddEntrySwitch: UISwitch!
    @IBOutlet weak var permanentEntrySwitch: UISwitch!
    @IBOutlet weak var darkModeSwitch: UISwitch!
    @IBOutlet weak var documentBackup: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pullToAddEntrySwitch.setOn(settings_UD.bool(forKey: "pullToAddEntry"), animated: true)
        permanentEntrySwitch.setOn(settings_UD.bool(forKey: "permanentEntries"), animated: true)
        darkModeSwitch.setOn(settings_UD.bool(forKey: "darkMode"), animated: true)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ThemeManager.applyTheme(view: self)
       
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let _ = segue.identifier else {
            return
        }
        let destView = segue.destination as! SettingsSelectionViewController
        destView.viewSegue = SegueIdentifier(rawValue: segue.identifier!)
    }

    @IBAction func pullToAddUpdated(_ sender: Any) {
        let newVal = pullToAddEntrySwitch.isOn
        settings_UD.set(newVal, forKey: "pullToAddEntry")
        print(newVal)
    }
    @IBAction func permanentEntriesChanged(_ sender: Any) {
        let newVal = permanentEntrySwitch.isOn
        settings_UD.set(newVal, forKey: "permanentEntries")
    }
    @IBAction func themeModeChanged(_ sender: Any) {
        let newVal = darkModeSwitch.isOn
        settings_UD.set(newVal, forKey: "darkMode")
        ThemeManager.applyTheme(view: self)
        if let root = UIApplication.shared.windows.first!.rootViewController as? UISplitViewController {
            print("got root")
            let navController = root.viewControllers[1] as! UINavigationController
            let jcv = navController.viewControllers[0] as! JournalCollectionViewController
            ThemeManager.applyTheme(view: jcv)
        } else {
            print("failed to get root")
        }


    }
}

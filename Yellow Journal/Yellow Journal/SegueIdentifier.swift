//
//  SegueIdentifier.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 11/2/20.
//

import Foundation

enum SegueIdentifier : String {
    case themesSegue = "themesSegue"
    case statusBarSegue = "statusBarSegue"
    case toolPositionSegue = "toolPositionSegue"
    case documentBackupSegue = "documentBackupSegue"
}

//
//  AddJournalViewController.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 10/27/20.
//

import Foundation
import UIKit
import CryptoKit

protocol NewJournalDelegate : class {
    func newJournal(journal: Journal)
}

class AddJournalViewController : UITableViewController, PasswordDelegate, CoverDelegate {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var journalTitle: UITextField!
    @IBOutlet weak var addJournalButton: UIBarButtonItem!
    
    var hasPassword : Bool = false
    var password : String? = nil
    var cover : UIImage? = nil
    
    weak var delegate: NewJournalDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addJournalButton.target = self
        addJournalButton.action = #selector(addJournalAction(sender:))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "passwordSegue" {
            let destView = segue.destination as! PasswordViewController
            destView.delegate = self
        }
        if segue.identifier == "coverSegue" {
            let destView = segue.destination as! JournalCoverCollectionViewController
            destView.delegate = self
        }
    }
    
    @objc func addJournalAction(sender: UIBarButtonItem) {
        guard let journal = createJournal() else {
            return
        }
        delegate?.newJournal(journal: journal)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func createJournal() -> Journal? {
        guard let title = self.journalTitle.text else {
            return nil
        }
        if title.isEmpty { return nil }
        if self.cover == nil {
            self.cover = #imageLiteral(resourceName: "yellowJournal")
        }
        if hasPassword {
            return Journal(name: title, coverArt: self.cover!, createdDate: Date(), password: password!, context: context)
        } else {
            return Journal(name: title, coverArt: self.cover!, createdDate: Date(), password: "-1", context: context)
        }
        
    }
    
    func userEnteredPassword(password: String) {
        self.password = password
        self.hasPassword = true
    }
    
    func userSelectedCover(cover: UIImage) {
        self.cover = cover
    }
}

//
//  Utils.swift
//  Yellow Journal
//
//  Created by Erik Gregorio on 11/11/20.
//

import Foundation
import UIKit

class Utils {
    static var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    static var settings = UserDefaults.standard

    
    static func displayError(error: String, view: UIViewController){
        let alert = UIAlertController(title: "Something went wrong!", message: error, preferredStyle: UIAlertController.Style.alert)

        // add ok button
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

        // show the alert
        view.present(alert, animated: true, completion: nil)
    }
    
    static func displayMessage(error: String, view: UIViewController){
        let alert = UIAlertController(title: "Request Successful!", message: error, preferredStyle: UIAlertController.Style.alert)
        // add ok button
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        // show the alert
        view.present(alert, animated: true, completion: nil)
    }
    
    static func requestPassword(view: UIViewController){
        let alert = UIAlertController(title: "Jornal is Protected!", message: "Please type your journal's password", preferredStyle: UIAlertController.Style.alert)
        let textField = UITextField()
        textField.placeholder = "Password"
        alert.addTextField(configurationHandler: configurationTextField)
    
        // add ok button
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        // show the alert
        view.present(alert, animated: true, completion: nil)
    }
    
    static func configurationTextField(textField: UITextField!){
        textField.placeholder = "Password"
        }
    
    static func intializeSettings(){
        let defaults = UserDefaults.standard
        let isPreloaded = defaults.bool(forKey: "settingExists")
        if !isPreloaded {
            defaults.set(false, forKey: "pullToAddEntry")
            defaults.set(false, forKey: "permanentEntries")
            defaults.set(false, forKey: "darkMode")
            defaults.set(true, forKey: "settingExists")
        }
        
    }
    
    static func isPullToAddEnabled() -> Bool{
        return settings.bool(forKey: "pullToAddEntry")
    }
    
    static func isPermanentEntriesEnabled() -> Bool{
        return settings.bool(forKey: "permanentEntries")
    }
    
    static func isDarkModeEnable() -> Bool{
        return settings.bool(forKey: "darkMode")
    }
}

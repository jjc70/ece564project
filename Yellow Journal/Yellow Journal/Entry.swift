//
//  Entry.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 10/14/20.
//

import PencilKit

class Entry {
    var name : String
    var createdDate : Date
    var lastModifiedDate : Date
    var content : PKDrawing
    var preview : String
    
    init(name : String,
         createdDate : Date,
         content : PKDrawing) {
        self.name = name
        self.createdDate = createdDate
        self.lastModifiedDate = createdDate
        self.content = content
        self.preview = ""
    }
    
    func getName() -> String {
        return self.name
    }
    
    func setName(_ name : String) {
        self.name = name
    }
    
    func getCreatedDate() -> Date {
        return self.createdDate
    }
    
    func setCreatedDate(_ date : Date) {
        self.createdDate = date
    }
    
    func getLastModifiedDate() -> Date {
        return lastModifiedDate
    }
    
    func setLastModifiedDate(_ date : Date) {
        self.lastModifiedDate = date
    }
    
    func getContent() -> PKDrawing {
        return self.content
    }
    
    func setContent(_ content : PKDrawing) {
        self.content = content
    }
    
    func getPreview() -> String {
        return self.preview
    }
    
    func setPreview(_ preview : String) {
        self.preview = preview
    }
}

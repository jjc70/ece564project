//
//  EntryViewController.swift
//  Yellow Journal
//
//  Created by Erik Gregorio on 10/27/20.
//

import UIKit
import PencilKit

class EntryViewController: UIViewController, UITextViewDelegate {

    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    @IBOutlet weak var addTextField: UIButton!
    @IBOutlet weak var enablePencilButton: UIBarButtonItem!
    @IBOutlet weak var navigationBar: UINavigationItem!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet var upRecognizer: UISwipeGestureRecognizer!
    
    private var journal : Journal?
    private var currentEntry : JournalEntry?
    
    var isAddingBox = false
    var isEditingText = false
    var textBeingEdited: UITextView?
    var isDrawing = false
    let canvasView = PKCanvasView(frame: .zero)
    let toolPicker = PKToolPicker.init()
    
    var textBoxes: [JournalTextBox] = []
    var textViews: [UITextView] = []
    var textMap: [UITextView:JournalTextBox] = [:]
    
    func setJournal(journal: Journal) {
        self.journal = journal
        if(self.journal?.lastModifiedEntry != nil){
            print("HAS LAST MODIFIED")
            currentEntry = self.journal?.lastModifiedEntry
        } else {
            print("CREATING A NEW ENTRY")

            let today = Date()
            print(today)
            currentEntry = JournalEntry(name: "", createdDate: today, content: canvasView.drawing, context: context)
            journal.addToEntries(currentEntry!)
        }
    }
    
    func setEntry(entry: JournalEntry) {
        print("trying my best to replace current entry")
        saveEntry()
        self.currentEntry = entry
        self.setUpEntryView()
    }
    
    func deleteEntry(entry: JournalEntry){
        if(entry == currentEntry){
            self.currentEntry = (journal?.entries?.lastObject)! as? JournalEntry
            self.setUpEntryView()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        ThemeManager.applyTheme(view: self)
        //self.view.backgroundColor = UIColor.black

        self.addTapDetection()
        
        enablePencilButton.target = self
        enablePencilButton.action = #selector(togglePencil)
        // Do any additional setup after loading the view.\
        // set up subview for pkcanvas
        canvasView.translatesAutoresizingMaskIntoConstraints = false
        canvasView.isUserInteractionEnabled = false
        canvasView.drawingPolicy = PKCanvasViewDrawingPolicy.anyInput
        view.addSubview(canvasView)
        
    
        
        NSLayoutConstraint.activate([
            canvasView.topAnchor.constraint(equalTo: view.topAnchor),
            canvasView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            canvasView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            canvasView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
        
        if(currentEntry != nil){
            print("SETTING UP ENTRY")
            self.setUpEntryView()
        }
        // Set swipe up according to setting
        upRecognizer.isEnabled = Utils.isPullToAddEnabled()
        
        self.splitViewController?.preferredDisplayMode = UISplitViewController.DisplayMode.oneBesideSecondary
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        toolPicker.setVisible(true, forFirstResponder: canvasView)
        toolPicker.addObserver(canvasView)
    }
    
    @IBAction func enableBoxCreation(_ sender: Any) {
        if(isAddingBox){
            addTextField.isSelected = false;
            isAddingBox = false;
            textBeingEdited?.endEditing(true)
        } else {
            if(isDrawing){
                enablePencilButton.image = UIImage(systemName: "pencil.circle")
                isDrawing = false
                canvasView.isUserInteractionEnabled = false
                canvasView.resignFirstResponder()
                if(Utils.isPullToAddEnabled()){
                    upRecognizer.isEnabled = true
                }

            }
            isAddingBox = true;
            addTextField.isSelected = true;
                //UIButton.State.selected
        }
    }
    @IBAction func detectSwipeUp(_ sender: Any) {
        print("\n SWIPED UP \n")
        self.splitViewController?.preferredDisplayMode = UISplitViewController.DisplayMode.secondaryOnly
        let canv = PKCanvasView(frame: .zero)
        let entry = JournalEntry(name: "", createdDate: Date(), content: canv.drawing, context: self.context)
        journal!.addToEntries(entry)
        saveEntry()
        setEntry(entry: entry)
    }
    func addTapDetection() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.addTextBox(recognizer:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func togglePencil(sender: UIBarButtonItem) {
        if(isDrawing){
            sender.image = UIImage(systemName: "pencil.circle")
            isDrawing = false
            canvasView.isUserInteractionEnabled = false
            canvasView.resignFirstResponder()
            if(Utils.isPullToAddEnabled()){
                upRecognizer.isEnabled = true
            }

        } else {
            if(isAddingBox){
                textBeingEdited?.endEditing(true)
                addTextField.isSelected = false;
                isAddingBox = false;
            }
            sender.image = UIImage(systemName: "pencil.circle.fill")
            isDrawing = true
            canvasView.isUserInteractionEnabled = true
            canvasView.becomeFirstResponder()
            upRecognizer.isEnabled = false
        }
    }
    
    @objc func addTextBox(recognizer : UIGestureRecognizer) {
        //view.endEditing(true)
        // assume only text box addition right now
        if(isAddingBox){
            if(!isEditingText){
                let tappedPoint = recognizer.location(in: self.view)//[recognizer locationInView:self.view];
                let xCoordinate = tappedPoint.x
                let yCoordinate = tappedPoint.y
                let text = createTextBox(xCoordinate: xCoordinate, yCoordinate: yCoordinate)
                let textBox = JournalTextBox(context: context)
                textBox.xCoordinate = Float(xCoordinate)
                textBox.yCoordinate = Float(yCoordinate)
                textViews.append(text)
                textBoxes.append(textBox)
                textMap[text] = textBox
                self.view.addSubview(text)
                text.becomeFirstResponder()
            } else {
                textBeingEdited?.endEditing(true)
            }
        }
    }
    
    func createTextBox(xCoordinate : CGFloat, yCoordinate : CGFloat) -> UITextView{
        let text = UITextView(frame: CGRect(x: xCoordinate, y: yCoordinate, width: 200, height: 30))
        //label.backgroundColor = UIColor.blue
        //text.
        text.delegate = self
        text.translatesAutoresizingMaskIntoConstraints = true
        text.isScrollEnabled = false
        text.layer.borderColor = UIColor.systemBlue.cgColor
        text.layer.borderWidth = 1.0
        text.layer.cornerRadius = 8
        text.isUserInteractionEnabled = true
        return text;
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.layer.borderColor = UIColor.systemBlue.cgColor
        textView.layer.borderWidth = 1.0
        textView.layer.cornerRadius = 8
        self.isEditingText = true
        textBeingEdited = textView
        print("\n EDITING PLU IS DRAWING: \(isDrawing)\n")
        if(isDrawing){
            enablePencilButton.image = UIImage(systemName: "pencil.circle")
            isDrawing = false
            canvasView.isUserInteractionEnabled = false
            canvasView.resignFirstResponder()
            if(Utils.isPullToAddEnabled()){
                upRecognizer.isEnabled = true
            }
            isAddingBox = true;
            addTextField.isSelected = true;
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.layer.borderColor = UIColor.systemBlue.cgColor
        textView.layer.borderWidth = 0
        textView.layer.cornerRadius = 8
        self.isEditingText = false
        textMap[textView]?.textContent = textView.text
    }
//    func textViewDidChange(_ textView: UITextView) {
//        print("Changing TEXT!!! \n")
//        textView.sizeToFit()
//    }
    func textViewDidChange(_ textView: UITextView) {
          let fixedWidth = textView.frame.size.width
          textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
          let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
          var newFrame = textView.frame
          newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
          textView.frame = newFrame
    }
    
    
    func setUpEntryView(){
        // clear up boxes
        for textview in textViews {
            textview.removeFromSuperview()
        }
        textViews.removeAll()
        textBoxes.removeAll()
        textMap.removeAll()
        
        let textbxs = currentEntry?.textboxes?.array as! [JournalTextBox]
        
        for txtbx in textbxs {
            let text = createTextBox(xCoordinate: CGFloat(txtbx.xCoordinate), yCoordinate: CGFloat(txtbx.yCoordinate))
            text.delegate = self
            text.text = txtbx.textContent
            text.layer.borderWidth = 0
            textViews.append(text)
            textBoxes.append(txtbx)
            textMap[text] = txtbx
            self.view.addSubview(text)
        }
        
        // place drawing
        do {
            self.canvasView.drawing = try PKDrawing(data: (currentEntry?.content)!)
        } catch {
            print("Could not load drawing")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func saveEntry() {
        currentEntry?.content = canvasView.drawing.dataRepresentation()
        journal?.lastModifiedEntry = currentEntry
        for(_, tbox) in textMap {
            if(!(currentEntry?.textboxes?.contains(tbox))!){
                currentEntry?.addToTextboxes(tbox)
            }
        }
        do {
            try context.save()
        } catch {
            let alert = UIAlertController(title: "Something went wrong!", message: "Unable to save entry", preferredStyle: UIAlertController.Style.alert)
            // add ok button
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }

    @IBAction func toCollectionView(_ sender: Any) {
        // save the PKDrawing and set as last modified
        currentEntry?.content = canvasView.drawing.dataRepresentation()
        journal?.lastModifiedEntry = currentEntry
        for(_, tbox) in textMap {
            if(!(currentEntry?.textboxes?.contains(tbox))!){
                currentEntry?.addToTextboxes(tbox)
            }
        }
        self.splitViewController?.preferredDisplayMode = UISplitViewController.DisplayMode.secondaryOnly
        do {
            try context.save()
        } catch {
            let alert = UIAlertController(title: "Something went wrong!", message: "Unable to save entry", preferredStyle: UIAlertController.Style.alert)
            // add ok button
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        if let root = UIApplication.shared.windows.first!.rootViewController as? UISplitViewController {
            print("got root")
            let navController = root.viewControllers[0] as! UINavigationController
            let tvc = navController.viewControllers[0] as! PrimaryTableViewController
            tvc.clearEntries()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func saveJournal(){
        do{
            try context.save()
        } catch {
            let alert = UIAlertController(title: "Something went wrong!", message: "Unable to add new journal", preferredStyle: UIAlertController.Style.alert)
            // add ok button
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
        
}

//
//  Theme.swift
//  Yellow Journal
//
//  Created by Erik Gregorio on 11/12/20.
//

import UIKit
let SelectedThemeKey = "SelectedTheme"

struct ThemeManager {
    
    static func applyTheme(view : UIViewController) {
        if(Utils.isDarkModeEnable()){
            view.navigationController?.navigationBar.barTintColor = UIColor.black
            view.navigationController?.navigationBar.tintColor = UIColor.white
            view.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
            view.view.backgroundColor = UIColor.systemGray
            view.view.tintColor = UIColor.white
        } else {
            view.navigationController?.navigationBar.barTintColor = UIColor.white
            view.navigationController?.navigationBar.tintColor = UIColor.systemBlue
            view.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
            view.view.backgroundColor = UIColor.systemGroupedBackground
            view.view.tintColor = UIColor.blue
        }
    }

}

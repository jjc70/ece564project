//
//  JournalCoverViewCell.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 11/12/20.
//

import UIKit

class JournalCoverViewCell: UICollectionViewCell {
    @IBOutlet weak var journalCover: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.journalCover.image = nil
    }
}

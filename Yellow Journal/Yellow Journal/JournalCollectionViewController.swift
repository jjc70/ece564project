//
//  JournalCollectionViewController.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 10/14/20.
//

import UIKit

class JournalCollectionViewController : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISplitViewControllerDelegate, NewJournalDelegate {
    
    //MARK: Properties
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    @IBOutlet var journalCollectionView: UICollectionView!
    @IBOutlet weak var settingsButton: UIBarButtonItem!
    @IBOutlet weak var trashButton: UIBarButtonItem!
    @IBOutlet weak var addJournalButton: UIBarButtonItem!
    
    private let reuseIdentifier = "JournalCell"
    private let numCols = 5
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    private var isInTrashMode : Bool = false
    private var journals : [Journal] = []
    private var currJournal : Journal?
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let flowLayout = self.journalCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.itemSize = CGSize(width: 202, height: 308)
            flowLayout.sectionInset = UIEdgeInsets(top: 15.0, left: 15.0, bottom: 15.0, right: 15.0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ThemeManager.applyTheme(view: self)
        updateCollectionData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateCollectionData()
        
        self.trashButton.target = self
        self.trashButton.action = #selector(trashButtonAction(sender:))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "newJournalSegue" {
            let navController = segue.destination as! UINavigationController
            let destView = navController.viewControllers.first as! AddJournalViewController
            destView.delegate = self
        }
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return journals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? JournalCell else {
            fatalError("The dequeued cell is not an instance of JournalCell")
        }
        
        let journal = journals[indexPath.item]
        cell.journalName.text = journal.getName()
        cell.journalCover.image = journal.getCoverArt()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isInTrashMode {
            self.context.delete(journals[indexPath.row])
            do{
                try context.save()
            } catch {
                let alert = UIAlertController(title: "Something went wrong!", message: "Unable to add new journal", preferredStyle: UIAlertController.Style.alert)
                // add ok button
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
            journals.remove(at: indexPath.row)
            journalCollectionView.reloadData()
            isInTrashMode = false
            trashButton.image = UIImage(systemName: "trash")
        } else if journals[indexPath.row].getPassword() != "-1" {
            let destView = storyboard?.instantiateViewController(identifier: "EnterPasswordViewController") as! EnterPasswordViewController
            destView.modalPresentationStyle = UIModalPresentationStyle.popover
            self.currJournal = journals[indexPath.row]
            destView.delegate = self
            destView.setJournalPassword(currJournal!.getPassword())
            let popOver : UIPopoverPresentationController = destView.popoverPresentationController!
            popOver.sourceView = journalCollectionView.cellForItem(at: indexPath)
            self.present(destView, animated: true, completion: nil)
        } else {
            self.currJournal = journals[indexPath.row]
            presentEntryView()
        }
    }
    
    func presentEntryView() {
        let destView = storyboard?.instantiateViewController(identifier: "EntryViewController") as! EntryViewController
        self.navigationController?.pushViewController(destView, animated: true)
        self.splitViewController?.preferredDisplayMode = UISplitViewController.DisplayMode.oneBesideSecondary
        self.splitViewController?.preferredPrimaryColumnWidthFraction = 0.30
        self.splitViewController?.maximumPrimaryColumnWidth = (splitViewController?.view.bounds.size.width)!
        self.splitViewController?.minimumPrimaryColumnWidth = 380
        let navController = self.splitViewController?.viewControllers.first as! UINavigationController
        let primView = navController.viewControllers.first as! PrimaryTableViewController
        primView.entries = currJournal!.entries?.array as! [JournalEntry]
        primView.journal = currJournal
        destView.setJournal(journal: currJournal!)
    }
    
    func collectionView(_ collectionView: UICollectionView, canEditItemAt indexPath: IndexPath) -> Bool {
        return isInTrashMode
    }
    
    func newJournal(journal: Journal) {
        updateContext()
        journals.append(journal)
        updateCollectionData()
    }
    
    func updateContext(){
        do{
            try context.save()
        } catch {
            let alert = UIAlertController(title: "Something went wrong!", message: "Unable to add new journal", preferredStyle: UIAlertController.Style.alert)
            // add ok button
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func updateCollectionData() {
        do {
            journals = try self.context.fetch(Journal.fetchRequest())
        } catch {
            print(error)
        }
        journalCollectionView.reloadData()
    }
    
    @objc func trashButtonAction(sender: UIBarButtonItem) {
        if isInTrashMode {
            sender.image = UIImage(systemName: "trash")
        } else {
            sender.image = UIImage(systemName: "trash.fill")
        }
        isInTrashMode = !isInTrashMode
    }
}

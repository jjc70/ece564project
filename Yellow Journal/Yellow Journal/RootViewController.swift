//
//  RootViewController.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 11/9/20.
//

import UIKit

class RootViewController: UISplitViewController {
    var displayModeButton : UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = nil
    }
    
    /*
     MARK: - Navigation

     In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         Get the new view controller using segue.destination.
         Pass the selected object to the new view controller.
    }
    */

}

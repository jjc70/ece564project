//
//  PrimaryTableViewController.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 10/15/20.
//

import Foundation
import UIKit
import PencilKit

class PrimaryTableViewController : UITableViewController {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet var primaryTableView: UITableView!
    @IBOutlet weak var entryButton: UIBarButtonItem!
    
    var journal : Journal?
    var entries : [JournalEntry] = []
    
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        formatter.dateFormat = "dd MMM yyyy HH:mm:ss"
        
        entryButton.target = self
        entryButton.action = #selector(addEntryAction(sender:))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(journal != nil){
            entries = journal?.entries?.array as! [JournalEntry]
        } else {
            entries = []
        }
        ThemeManager.applyTheme(view: self)
        primaryTableView.reloadData()

    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entries.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "PrimaryTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PrimaryTableViewCell else {
            fatalError("The dequed call is not an instance of PersonTableViewCell")
        }
        cell.name.text = formatter.string(from: entries[indexPath.row].getCreatedDate())
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navController = self.splitViewController?.viewControllers[1] as! UINavigationController
        let entryView = navController.viewControllers[1] as! EntryViewController
        entryView.saveEntry()
        entryView.setEntry(entry: entries[indexPath.row])
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if(Utils.isPermanentEntriesEnabled()){
                Utils.displayError(error: "Permanent Entries are currently enabled. To delete, please disable it via settings", view: self)
            } else {
                journal?.removeFromEntries(entries[indexPath.row])
                let navController = self.splitViewController?.viewControllers[1] as! UINavigationController
                let entryView = navController.viewControllers[1] as! EntryViewController
                entryView.deleteEntry(entry: entries[indexPath.row])
                context.delete(entries[indexPath.row])
                entries = journal?.entries?.array as! [JournalEntry]
                do{
                    try context.save()
                } catch {
                    let alert = UIAlertController(title: "Something went wrong!", message: "Unable to add new journal", preferredStyle: UIAlertController.Style.alert)
                    // add ok button
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                }
                primaryTableView.reloadData()
            }
        }
    }
    
    @objc func addEntryAction(sender: UIBarButtonItem) {
        let entry = JournalEntry(name: "", createdDate: Date(), content: PKCanvasView(frame: .zero).drawing, context: self.context)
        journal!.addToEntries(entry)
        entries = journal?.entries?.array as! [JournalEntry]
        primaryTableView.reloadData()
        do{
            try context.save()
        } catch {
            let alert = UIAlertController(title: "Something went wrong!", message: "Unable to add new journal", preferredStyle: UIAlertController.Style.alert)
            // add ok button
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        let navController = self.splitViewController?.viewControllers[1] as! UINavigationController
        let entryView = navController.viewControllers[1] as! EntryViewController
        entryView.setEntry(entry: entry)
    }
    
    func clearEntries(){
        journal = nil
    }
}

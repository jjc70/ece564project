//
//  PrimaryTableViewCell.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 10/15/20.
//

import Foundation
import UIKit

class PrimaryTableViewCell : UITableViewCell {
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

//
//  PasswordViewController.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 11/2/20.
//

import Foundation
import UIKit

protocol PasswordDelegate : class {
    func userEnteredPassword(password: String)
}

class PasswordViewController : UITableViewController {
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    @IBOutlet weak var setPasswordButton: UIBarButtonItem!
    
    weak var delegate : PasswordDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setPasswordButton.target = self
        setPasswordButton.action = #selector(sendPasswordHash(sender:))
    }
    
    @objc func sendPasswordHash(sender: UIBarButtonItem) {
        if passwordsMatch(strA: passwordField.text!, strB: confirmPasswordField.text!) {
            delegate?.userEnteredPassword(password: passwordField.text!)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func passwordsMatch(strA: String, strB:String) -> Bool {
        if strA.count < 8 {
            passwordField.placeholder = "Password must be at least 8 characters"
            passwordField.text = ""
            confirmPasswordField.text = ""
            return false
        }
        if strA != strB {
            passwordField.placeholder = "Passwords must match"
            passwordField.text = ""
            confirmPasswordField.text = ""
            return false
        }
        return true
    }
}

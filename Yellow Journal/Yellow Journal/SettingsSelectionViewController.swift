//
//  SettingsSelectionViewController.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 11/2/20.
//

import Foundation
import UIKit

class SettingsSelectionViewController : UIViewController {
    var viewSegue : SegueIdentifier?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch viewSegue {
        case .themesSegue:
            view.backgroundColor = .black
        case .statusBarSegue:
            view.backgroundColor = .blue
        case .toolPositionSegue:
            view.backgroundColor = .orange
        case .documentBackupSegue:
            view.backgroundColor = .gray
        default:
            // if error occurs, return to root view
            _ = navigationController?.popToRootViewController(animated: true)
        }
    }
    
    override func loadView() {
        super.loadView()
    }
}

////
////  Journal.swift
////  Yellow Journal
////
////  Created by Januário José Blanc Carreiro on 10/14/20.
////
//
//import Foundation
//import UIKit
//import CryptoKit
//
//class Journal {
//    private var name : String
//    private var coverArt : UIImage
//    private var entries : [Entry]
//    private var createdDate : Date
//    private var lastModifedDate : Date
//    private var hashedPassword : Int
//    
//    init(name : String,
//         coverArt : UIImage,
//         entries : [Entry],
//         createdDate : Date,
//         hashedPassword : Int) {
//        self.name = name
//        self.coverArt = coverArt
//        self.entries = entries
//        self.createdDate = createdDate
//        self.lastModifedDate = createdDate
//        self.hashedPassword = hashedPassword
//    }
//    
//    func getName() -> String {
//        return self.name
//    }
//    
//    func setName(_ name : String) {
//        self.name = name
//    }
//    
//    func getCoverArt() -> UIImage {
//        return self.coverArt
//    }
//    
//    func setCoverArt(_ art : UIImage) {
//        self.coverArt = art
//    }
//    
//    func getEntries() -> [Entry] {
//        return self.entries
//    }
//    
//    func setEntries(_ entries : [Entry]) {
//        self.entries = entries
//    }
//    
//    func getCreatedDate() -> Date {
//        return self.createdDate
//    }
//    
//    func setCreatedDate(_ date : Date) {
//        self.createdDate = date
//    }
//    
//    func getLastModifiedDate() -> Date {
//        return self.lastModifedDate
//    }
//    
//    func setLastModifiedDate(_ date : Date) {
//        self.lastModifedDate = date
//    }
//    
//    func getHashedPassword() -> Int {
//        return self.hashedPassword
//    }
//    
//    func setHashedPassword(_ hashedPassword : Int) {
//        self.hashedPassword = hashedPassword
//    }
//}

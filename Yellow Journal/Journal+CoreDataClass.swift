//
//  Journal+CoreDataClass.swift
//  Yellow Journal
//
//  Created by Januário José Blanc Carreiro on 11/5/20.
//
//

import Foundation
import UIKit
import CoreData

@objc(Journal)
public class Journal: NSManagedObject {
    convenience init(name : String,
                     coverArt : UIImage,
                     createdDate : Date,
                     password : String,
                     context : NSManagedObjectContext) {
        self.init(context: context)
        self.name = name
        self.coverArt = coverArt.jpegData(compressionQuality: 1)!
        self.createdDate = createdDate
        self.lastModifiedDate = createdDate
        self.password = password
    }
    
    func getName() -> String {
        return self.name!
    }
    
    func setName(_ name : String) {
        self.name = name
    }
    
    func getCoverArt() -> UIImage {
        return UIImage(data: self.coverArt!)!
    }
    
    func setCoverArt(_ art : UIImage) {
        self.coverArt = art.jpegData(compressionQuality: 1)!
    }
    
    func getCreatedDate() -> Date {
        return self.createdDate!
    }
    
    func setCreatedDate(_ date : Date) {
        self.createdDate = date
    }
    
    func getLastModifiedDate() -> Date {
        return self.lastModifiedDate!
    }
    
    func setLastModifiedDate(_ date : Date) {
        self.lastModifiedDate = date
    }
    
    func getPassword() -> String {
        return self.password!
    }
    
    func setPassword(_ password : String) {
        self.password = password
    }
}

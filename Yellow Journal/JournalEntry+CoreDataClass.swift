//
//  JournalEntry+CoreDataClass.swift
//  Yellow Journal
//
//  Created by Erik Gregorio on 11/5/20.
//
//

import Foundation
import CoreData
import PencilKit

@objc(JournalEntry)
public class JournalEntry: NSManagedObject {
    convenience init(name : String,
         createdDate : Date,
         content : PKDrawing,
         context : NSManagedObjectContext) {
        self.init(context: context)
        self.name = name
        self.createdDate = createdDate
        self.lastModifiedDate = createdDate
        self.content = content.dataRepresentation()
        self.preview = ""
    }
    
    func getName() -> String {
        return self.name!
    }
    
    func setName(_ name : String) {
        self.name = name
    }
    
    func getCreatedDate() -> Date {
        return self.createdDate!
    }
    
    func setCreatedDate(_ date : Date) {
        self.createdDate = date
    }
    
    func getLastModifiedDate() -> Date {
        return lastModifiedDate!
    }
    
    func setLastModifiedDate(_ date : Date) {
        self.lastModifiedDate = date
    }
    
    func getContent() -> PKDrawing {
        do {
            return try PKDrawing(data: self.content!)
        } catch {
            
        }
        return PKDrawing.init()
    }
    
    func setContent(_ content : PKDrawing) {
        self.content = content.dataRepresentation()
    }
    
    func getPreview() -> String {
        return self.preview!
    }
    
    func setPreview(_ preview : String) {
        self.preview = preview
    }
}
